class Node{
    int item;
    Node next;
    Node(){
        item = 0;
        next = null;
    }
    Node(int x_item){
        item = x_item;
        next = null;
    }
}

class Stack{
    private Node top;
    private int size;

    Stack(){
        size = 0;
        top = null;
    }

    void push(int x_item) {
        if(size == 0)
            top = new Node(x_item);
        else{
            Node temp = new Node(x_item);
            temp.next = top;
            top = temp;
        }
        size++;
    }


    void pop(){
        if(size > 0)
        {
            top = top.next;
            size--;
        }
    }


    boolean isEmpty(){
        if(size > 0 )
        {
            System.out.println("Stack is not empty");
            return false;
        }
        System.out.println("Stack is empty");
        return true;
    }

    void numOfElement(){
        System.out.println("Stack size: "+size);
    }

    void search(int x_item){
        int index = 0;
        Node cur = top;
        while (cur != null) {
            index++;
            if(cur.item == x_item){
                System.out.println("Index: "+index);
                return;
            }
            cur = cur.next;
        }
    }

    void display(){
        Node temp = top;
        while(temp != null)
        {
            System.out.print(temp.item + " ");
            temp = temp.next;
        }
        System.out.println();
    }
}

public class test {
    public static void main(String[] args){
        Stack a = new Stack();
        a.isEmpty();
        int i = 10;
        while (i > 0) {
            a.push(i);
            i--;
        }
        a.display();
        while (i < 5){
            a.pop();
            i++;
        }
        a.display();
        a.numOfElement();
        a.isEmpty();
        a.push(4);
        a.push(3);
        a.search(8);
        a.display();
    }
}