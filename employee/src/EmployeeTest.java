
public class EmployeeTest {

	public static void main(String[] args) {
		
		Employee emp1 = new Employee();
		emp1.setFirstName("A");
		emp1.setLastName("B");
		emp1.setMSalary(100);
		System.out.println("Details Employee 1:");
		System.out.println("First Name: " + emp1.getFirstName());
		System.out.println("Last Name: " + emp1.getLastName());
		System.out.println("Month Salary: " + emp1.getMSalary());
		double yearSalary1 = emp1.getMSalary() * 12;
		System.out.println("Year Salary: " + yearSalary1);
		double raiseSalary1 = yearSalary1 * 1.1;
		System.out.println("10% Year Salary: " + raiseSalary1);
		
		System.out.println("\n");
		System.out.println("--------------------------------------------");
		System.out.println("\n");
		
		Employee emp2 = new Employee();
		emp2.setFirstName("C");
		emp2.setLastName("D");
		emp2.setMSalary(1000);
		System.out.println("Details Employee 2:");
		System.out.println("First Name: " + emp2.getFirstName());
		System.out.println("Last Name: " + emp2.getLastName());
		System.out.println("Month Salary: " + emp2.getMSalary());
		double yearSalary2 = emp2.getMSalary() * 12;
		System.out.println("Year Salary: " + yearSalary2);
		double raiseSalary2 = yearSalary2 * 1.1;
		System.out.println("10% Year Salary: " + raiseSalary2);
		
		System.out.println("\n");
		System.out.println("--------------------------------------------");
		System.out.println("\n");
		
		Employee emp3 = new Employee();
		emp3.setFirstName("");
		emp3.setLastName("");
		emp3.setMSalary(-1);
		System.out.println("Details Employee 3:");
		System.out.println("First Name: " + emp3.getFirstName());
		System.out.println("Last Name: " + emp3.getLastName());
		System.out.println("Month Salary: " + emp3.getMSalary());
		double yearSalary3 = emp3.getMSalary() * 12;
		System.out.println("Year Salary: " + yearSalary3);
		double raiseSalary3 = yearSalary3 * 1.1;
		System.out.println("10% Year Salary: " + raiseSalary3);
	}

}
