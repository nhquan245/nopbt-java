
public class Employee {
	private String firstName;
	private String lastName;
	private double m_salary;
	
	public void setFirstName(String a){
		firstName = a;
	}
	public String getFirstName(){
		return firstName;
	}
	
	public void setLastName(String a){
		lastName = a;
	}
	public String getLastName(){
		return lastName;
	}
	
	public void setMSalary(double a){
		if(a > 0) m_salary = a;
		else m_salary = 0.0;
	}
	public double getMSalary(){
		return m_salary;
	}
	
	public Employee() {
		firstName = null;
		lastName = null;
		m_salary = 0.0;
	}

}
