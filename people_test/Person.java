package people_test;

public class Person
{
    protected	String name;
    protected	Date birthday;

    public Person (String name, Date birth){
        this.name = name;
        this.birthday = birth;
    }
    public String getName(){
        return name;
    }
    public String toString(){
        return name + " " + birthday;
    }
}
