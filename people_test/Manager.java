package people_test;

public class Manager extends Employee
{
    protected Employee assistant;

    public Manager (String name, Date birth, double salary){
        super(name, birth,salary);
    }
    public void setAssistant(Employee tmp){
        assistant = tmp;
    }
    public String toString(){
        return String.format("%s --- Assistant: %s", super.toString(), assistant.getName());
    }
}
