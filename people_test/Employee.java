package people_test;

public class Employee extends Person
{
    protected	double salary;

    public Employee (String name, Date birth, double salary){
        super(name, birth);
        this.salary = salary;
    }
    public void setEmployee(String name, Date birth, double salary){
        this.name = name;
        this.birthday = birth;
        this.salary = salary;
    }
    public double getSalary(){
        return salary;
    }
    public String toString(){
        return super.toString() +" "+ salary;
    }
}
