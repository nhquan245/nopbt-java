package people_test;

public class Date
{
    protected int month;
    protected int day;
    protected int year;


    public Date( int Day, int Month, int Year )
    {
        month = check_month( Month );
        year = Year;
        day = check_day( Day );
    }
    public void nextDay (){
        day = day+1;
        day = check_day(day);
        if(day==1){
            month = month+1;
            month = check_month(month);
            if(month == 1) year = year +1;
        }
    }
    private int check_month( int test_m )
    {
        if ( test_m > 0 && test_m <= 12 )
            return test_m;
        else
        {
            return 1;
        }
    }
    private int check_day( int test_d )
    {
        int days_in_month[] =
                { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };


        if ( test_d > 0 && test_d <= days_in_month[ month ] )
            return test_d;
        if ( month == 2 && test_d == 29 && ( year % 400 == 0 ||
                ( year % 4 == 0 && year % 100 != 0 ) ) )
            return test_d;
        return 1;
    }
    public String toString() //  mm/dd/yyyy
    {
        return String.format( "%d/%d/%d", month, day, year );
    }
}